package com.example.starter_git.services;

import com.example.starter_git.models.MalwareURL;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class UrlHausService implements GatherInfoService{

    private final RestTemplate restTemplate;
    private final String BASIC_ADDR = "https://urlhaus-api.abuse.ch/v1/urls/recent/";


    public MalwareURL getInfoMalware() {
        HttpEntity<?> httpEntity = RequestEntity.EMPTY;
        ResponseEntity<MalwareURL> responseEntity =
                restTemplate.exchange(BASIC_ADDR, HttpMethod.GET, httpEntity, MalwareURL.class);
        return responseEntity.getBody();

    }
}

package com.example.starter_git.services;

import com.example.starter_git.models.MalwareURL;

public interface GatherInfoService {

    MalwareURL getInfoMalware();

}
